#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#define sh_isspace(c) \
    ((c==' ')||(c>='\t'&&c<='\r'))
	
#define CERR(fmt, ...) \
    fprintf(stderr,"[%s:%s:%d][error %d:%s]" fmt "\r\n",\
         __FILE__, __func__, __LINE__, errno, strerror(errno),##__VA_ARGS__)

#define CERR_EXIT(fmt,...) \
    CERR(fmt,##__VA_ARGS__),exit(EXIT_FAILURE)

#define IF_CHECK(code)    \
    if((code) < 0) \
        CERR_EXIT(#code)
        

#define _INT_BUF (1024)

#define _INT_LIS (7)


int getfdline(int fd, char buf[], int sz);


extern inline void response_400(int cfd);


extern inline void response_404(int cfd);


extern inline void response_501(int cfd);


extern inline void response_500(int cfd);


extern inline void response_200(int cfd);


void response_file(int cfd, const char* path);


int serstart(uint16_t* pport);


void* request_accept(void* arg);


void request_cgi(int cfd, const char* path, const char* type, const char* query);


int main(int argc, char* argv[])
{
    pthread_attr_t attr;
    uint16_t port = 0;
    int sfd = serstart(&port);
    
    printf("httpserver %u.\n", port);

    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    for(;;){
        pthread_t tid;
        struct sockaddr_in caddr;
        socklen_t clen = sizeof caddr;
        int cfd = accept(sfd, (struct sockaddr*)&caddr, &clen);
        if(cfd < 0){
            CERR("accept sfd = %d is error!", sfd);
            break;
        }
        if(pthread_create(&tid, &attr, request_accept, (void*)cfd) < 0)
            CERR("pthread_create run is error!");
    }

    pthread_attr_destroy(&attr);
    close(sfd);
    return 0;
}

int 
getfdline(int fd, char buf[], int sz)
{
    char* tp = buf;
    char c;
    
    --sz;
    while((tp-buf)<sz){
        if(read(fd, &c, 1) <= 0) 
            break;
        if(c == '\r'){ 
            if(recv(fd, &c, 1, MSG_PEEK)>0 && c == '\n')
                read(fd, &c, 1);
            else 
                *tp++ = '\n';
            break;
        }
        *tp++ = c;
    }
    *tp = '\0';
    return tp - buf;
}

inline void 
response_400(int cfd)
{
    const char* estr = "HTTP/1.0 400 BAD REQUEST\r\n";
    
    write(cfd, estr, strlen(estr));
}

inline void 
response_404(int cfd)
{
    const char* estr = "HTTP/1.0 404 NOT FOUND\r\n";
    
    write(cfd, estr, strlen(estr));
}

inline void 
response_501(int cfd)
{
    const char* estr = "HTTP/1.0 501 Method Not Implemented\r\n";
    
    write(cfd, estr, strlen(estr));
}


inline void 
response_500(int cfd)
{
    const char* estr = "HTTP/1.0 500 Internal Server Error\r\n";

    write(cfd, estr, strlen(estr));
}

inline void 
response_200(int cfd)
{
    const char* str = "HTTP/1.0 200 OK\r\n";
    
    write(cfd, str, strlen(str));
}

void 
response_file(int cfd, const char* path)
{
    FILE* txt;
    char buf[_INT_BUF];
    
    while(getfdline(cfd, buf, sizeof buf)>0 && strcmp("\n", buf))
        ;

    if((txt = fopen(path, "r")) == NULL) 
        response_404(cfd);
    else{
        response_200(cfd); 
        
        while(!feof(txt) && fgets(buf, sizeof buf, txt))
            write(cfd, buf, strlen(buf));
    }
    fclose(txt);
}

int 
serstart(uint16_t* pport)
{
    int sfd;
    struct sockaddr_in saddr = { AF_INET };
    
    IF_CHECK(sfd = socket(PF_INET, SOCK_STREAM, 0));
    saddr.sin_port = !pport || !*pport ? 0 : htons(*pport);
    saddr.sin_addr.s_addr = INADDR_ANY;

    IF_CHECK(bind(sfd, (struct sockaddr*)&saddr, sizeof saddr));
    if(pport && !*pport){
        socklen_t clen = sizeof saddr;
        IF_CHECK(getsockname(sfd, (struct sockaddr*)&saddr, &clen));
        *pport = ntohs(saddr.sin_port);
    }

    IF_CHECK(listen(sfd, _INT_LIS));
    return sfd;
}

void* 
request_accept(void* arg)
{
    char buf[_INT_BUF], path[_INT_BUF>>1], type[_INT_BUF>>5];
    char *lt, *rt, *query, *nb = buf;
    struct stat st;
    int iscgi, cfd = (int)arg;

    if(getfdline(cfd, buf, sizeof buf) <= 0){ 
        response_501(cfd);
        close(cfd);
        return NULL;
    }

    for(lt=type, rt=nb; !sh_isspace(*rt) && (lt-type)< sizeof type - 1; *lt++ = *rt++)
        ;
    *lt = '\0'; 
    if((iscgi = strcasecmp(type, "PUT")) && strcasecmp(type, "GET")){
        response_501(cfd);
        close(cfd);
        return NULL;
    }
    
    while(*rt && sh_isspace(*rt))
        ++rt;
   
    *path = '.';
    for(lt = path + 1; (lt-path)<sizeof path - 1 && !sh_isspace(*rt); *lt++ = *rt++)
        ;
    *lt = '\0'; 
    
    
    if(iscgi != 0){
        for(query = path; *query && *query != '?'; ++query)
            ;
        if(*query == '?'){
            iscgi = 0;
            *query++ = '\0';
        }
    }
    
    
    if(stat(path, &st) < 0){
        while(getfdline(cfd, buf, sizeof buf)>0 && strcmp("\n", buf))// 读取内容直到结束
            ;
        response_404(cfd);
        close(cfd);
        return NULL;
    }
    
    if ((st.st_mode & S_IXUSR) ||(st.st_mode & S_IXGRP) ||(st.st_mode & S_IXOTH))
        iscgi = 0;
    if(iscgi) 
        response_file(cfd, path);
    else
        request_cgi(cfd, path, type, query);
    
    close(cfd);
    return NULL;
}

void 
request_cgi(int cfd, const char* path, const char* type, const char* query)
{
    char buf[_INT_BUF];
    int pocgi[2], picgi[2];
    pid_t pid;
    int contlen = -1; 
    char c;
    
    if(strcasecmp(type, "PUT") == 0){
        while(getfdline(cfd, buf, sizeof buf)>0 && strcmp("\n", buf)){
            buf[15] = '\0';
            if(!strcasecmp(buf, "Content-Length:"))
                contlen = atoi(buf + 16);
        }
        if(contlen == -1){ 
            response_400(cfd);
            return;
        }
    } 
    else{ 
        while(getfdline(cfd, buf, sizeof buf)>0 && strcmp("\n", buf))
            ;
    }
    
    
    if(pipe(pocgi) < 0){
        response_500(cfd);
        return;
    }
    if(pipe(picgi) < 0){ 
        close(pocgi[0]), close(pocgi[1]);
        response_500(cfd);
        return;
    }
    if((pid = fork())<0){
        close(pocgi[0]), close(pocgi[1]);
        close(picgi[0]), close(picgi[1]);
        response_500(cfd);
        return;
    }
    
    if(pid == 0) {
        
        dup2(pocgi[1], STDOUT_FILENO); 
        dup2(picgi[0], STDIN_FILENO); 
        close(pocgi[0]);
        close(pocgi[1]);
        
        
        sprintf(buf, "REQUEST_METHOD=%s", type);
        putenv(buf);
        
        if(strcasecmp(buf, "PUT") == 0)
            sprintf(buf, "CONTENT_LENGTH=%d", contlen);
        else
            sprintf(buf, "QUERY_STRING=%s", query);
        putenv(buf);
        
        execl(path, path, NULL);
        
        
        exit(EXIT_SUCCESS);
    }
    
    write(cfd, "HTTP/1.0 200 OK\r\n", 17);
    close(pocgi[1]);
    close(picgi[0]);
    
    if(strcasecmp(type, "PUT") == 0){
        int i; 
        for(i=0; i<contlen; ++i){
            read(cfd, &c, 1);
            write(picgi[1], &c, 1);
        }
    }
    
    while(read(pocgi[0], &c, 1) > 0)
        write(cfd, &c, 1);
    
    close(pocgi[0]);
    close(picgi[1]);
    
    waitpid(pid, NULL, 0);
}